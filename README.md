# Add the following modification to server and gateway deployment YAMLs

```
    spec:
      securityContext:
        runAsUser: 10001
        runAsGroup: 10001
      dnsConfig:
        options:
        - name: ndots
          value: "1"
```

# Add this change to just the server deployment YAML
```
        volumeMounts:
        - mountPath: /opt/aquascans
          name: aquascans-tmp

      volumes:
      - name: aquascans-tmp
        emptyDir: {}
```
